using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class Agent {
    public const int CHROMOSOME_SIZE = 100;

    public const float BASE_FITNESS = 250; 
    public const float FINISHED_TRACK_MULTIPLIER = 3.5f;
    public const float POSITION_MULTIPLIER = 20;
    public const float TIME_MULTIPLIER = -2;
    public const float JUMP_MULTIPLIER = -5;

    public AgentInterface Interface;    //A reference to the current interface the agent is using (to run the track)

    public Chromosome chromosome = new Chromosome(CHROMOSOME_SIZE);

    public bool disabled { get; set; }  //whether the agent is disabled or enabled

    public bool finishedTrack = false;  //whether the agent has finished the track
    public int completionTime;          //completion time is in 'frames' the current frame is recorded when the agent finishes it's run
    public int numberOfJumpsMade = 0;   //count of how many jumps the agent made

    /// <summary>
    /// Only has to make the fitness calculation once per agent.
    /// Afterwards the calculation is not made again.
    /// </summary>
    private float? _fitness = null;
    public float fitness {
        get {
            if (_fitness == null) {
                float score = BASE_FITNESS;
                    
                score += (Interface.position * POSITION_MULTIPLIER);

                if (finishedTrack) {
                    score += (numberOfJumpsMade * JUMP_MULTIPLIER);
                    score += (completionTime * TIME_MULTIPLIER);
                    score *= FINISHED_TRACK_MULTIPLIER;
                }
                
                _fitness = score;
            }
            return (float)_fitness;
        }
        set { _fitness = value; }
    }

    /// <summary>
    /// Registers the agent interface to the agent
    /// </summary>
    /// <param name="interface"></param>
    public void Register(AgentInterface @interface) {
        Interface = @interface;
        GeneticAlgorithmManager.activeAgents.Add(this);
        GeneticAlgorithmManager.generationAgents.Add(this);
    }

    /// <summary>
    /// Reset is called by survivors who need to reset their scores.
    /// This prevents additional jumps, etc being added to their fitness score on future runs
    /// </summary>
    public void Reset() {
        _fitness = null;
        completionTime = 0;
        finishedTrack = false;
        numberOfJumpsMade = 0;
    }
}
