using System;
using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class acts as the interface between the Unity game engine and the Genetic Algorithm we created.
/// Our agent class will hook into an interface and the interface uses the agents chromosome to determine
/// its next actions while running through a track.
///
/// Interfaces are never permanently destroyed after a generation run to save on processing power they
/// are reused for many different agents throughout the training process
/// </summary>
public class AgentInterface : MonoBehaviour {
    public new CustomRigidbody rigidbody;   //reference to the physics rigidbody

    public float movementSpeed = 10;    //how fast the agent moves
    public float jumpHeight = 25;

    [Range(0.2f, 1.5f)]
    public float airSpeedMultiplier = 0.75f;    //movement speed while traveling in the air
    //Keeps track of whether or not the second jump has been used yet. this is reset when the agent lands at any point
    private bool _secondJumpUsed = false;       

    [NonSerialized]
    public Agent agent; //reference to the current agent controlling the agent interface

    private bool _grounded = false; //whether or not the agent is touching the ground

    //gets the agents current position (converts the float position into an int
    public int position { 
        get { return Mathf.FloorToInt(this.transform.position.x); }
    }

    private void OnEnable() {
        //When the agent interface is re-enabled it hooks into its physics rigidbody
        this.rigidbody.postCollision += PostCollision;
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (agent == null) return;  //if there is no agent just end
        
        if (_grounded) {
            this.rigidbody.velocity.SetX(movementSpeed);
        }
        else {
            this.rigidbody.velocity.SetX(movementSpeed*airSpeedMultiplier);
        }
        
        if(agent.chromosome[position]) {    //if chromosome is true, jump
            Jump();
        }   
        else {  //else do not jump
            StopJump();
        }

        rigidbody.DoWork(); //moves the rigidbody through the track checking for collisions and other things
    }

    private const int MAX_FRAME_STUCK = 5;
    private Vector2 _lastPosition = Vector2.zero;
    private int _watchdog = MAX_FRAME_STUCK; //gets stuck for 3 frames and gets removed
    protected void PostCollision(CollisionInfo collision)
    {
        _grounded = collision.below; //if there was a collision below, the agent is grounded

        if (_grounded)
            _secondJumpUsed = false;

        if (this.transform.position.y < -10)
        {
            this.gameObject.SetActive(false);
        }
        
        //If the agent is past the finish line
        if (this.transform.position.x > GeneticAlgorithmManager.finishLine) {
            agent.finishedTrack = true;
            this.gameObject.SetActive(false);
        }

        //If the agent got stuck compare its position to the last position
        //Math.Abs is used to compare the float values because of floating point precision error.
        if ((Math.Abs(this.transform.position.x - _lastPosition.x) < 0.0001f) &&
            (Math.Abs(this.transform.position.y - _lastPosition.y) < 0.0001f))
        {
            _watchdog--;
            if (_watchdog < 0)
            {
                this.gameObject.SetActive(false);
            }
        }
        else
        {
            _lastPosition = this.transform.position;
            _watchdog = MAX_FRAME_STUCK;
        }

        //if the current frame is at the frame limit for the incremental GA
        if (GeneticAlgorithmManager.frameCount >= GeneticAlgorithmManager.incrementFrameLimit) {
            this.gameObject.SetActive(false);
        }
    }

    public void Jump() {
        if (_grounded) {
            agent.numberOfJumpsMade++;
            this.rigidbody.velocity.SetY(jumpHeight);
        }
        else if (!_secondJumpUsed && this.rigidbody.velocity.y <= 0) {
            _secondJumpUsed = true;
            agent.numberOfJumpsMade++;
            this.rigidbody.velocity.SetY(jumpHeight);
        }
    }

    public void StopJump() {
        if (!_grounded && this.rigidbody.velocity.y > 0) {
            this.rigidbody.velocity.SetY(0);
        }
    }

    public void NewGenerateAgent() {
        agent = new Agent();
        agent.chromosome.Scramble();
        agent.Register(this);
    }

    public void LoadAgent(Agent @agent) {
        @agent.Register(this);
        this.agent = @agent;
        _watchdog = MAX_FRAME_STUCK;
    }

    /// <summary>
    /// When the agent interface is disabled mark the Agent as completed
    /// </summary>
    private void OnDisable() {
        this.rigidbody.velocity.Set(Vector2.zero);
        this.rigidbody.postCollision -= PostCollision;

        if(agent != null) 
            agent.completionTime = GeneticAlgorithmManager.frameCount;

        GeneticAlgorithmManager.AgentFinished(agent);
    }
}
