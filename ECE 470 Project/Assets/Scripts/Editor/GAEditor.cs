using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GAEditor {

    [MenuItem("Genetic Algorithm/Train Agents", false, 0)]
    public static void StartRun() {
        GeneticAlgorithmManager.showingBestRun = false;

        if(GeneticAlgorithmManager.currentGeneration == 0)
            GeneticAlgorithmManager.Instance.StartNewGeneration();
    }

    [MenuItem("Genetic Algorithm/Train Agents", true, 0)]
    public static bool StartValidator() {
        return (Application.isPlaying && (GeneticAlgorithmManager.currentGeneration == 0 || GeneticAlgorithmManager.showingBestRun));
    }

    [MenuItem("Genetic Algorithm/Show Very Best Only", false, 1)]
    public static void DisplayBest() {
        GeneticAlgorithmManager.showingBestRun = true;
    }

    [MenuItem("Genetic Algorithm/Show Very Best Only", true, 1)]
    public static bool DisplayBestValidator() {
        return GeneticAlgorithmManager.veryBestAgentEver != null && !GeneticAlgorithmManager.showingBestRun;
    }

    [MenuItem("Genetic Algorithm/Training Frame Rate", false, 50)]
    public static void TrainingFrameRate() {
        GeneticAlgorithmManager.Instance.training = true;
        Time.fixedDeltaTime = GeneticAlgorithmManager.TRAINING_RATE;
        Debug.Log("Training Speed!");
    }
    [MenuItem("Genetic Algorithm/Training Frame Rate", true, 50)]
    public static bool TrainingFrameRateValidator() {
        return !GeneticAlgorithmManager.Instance.training;
    }

    [MenuItem("Genetic Algorithm/Watchable Frame Rate", false, 51)]
    public static void WatchFrameRate() {
        GeneticAlgorithmManager.Instance.training = false;
        Time.fixedDeltaTime = GeneticAlgorithmManager.WATCHABLE_RATE;
        Debug.Log("Enjoy!");
    }
    [MenuItem("Genetic Algorithm/Watchable Frame Rate", true, 51)]
    public static bool WatchFrameRateValidator() {
        return GeneticAlgorithmManager.Instance.training;
    }

    [MenuItem("Genetic Algorithm/Select Manager", false, 101)]
    public static void SelectManager() {
        Selection.activeGameObject = GeneticAlgorithmManager.Instance.gameObject;
    }

    [MenuItem("Genetic Algorithm/Print Log", false, 100)]
    public static void PrintLog() {
        GeneticAlgorithmManager.Instance.log.PrintToTextFile("fitness");
    }
}
