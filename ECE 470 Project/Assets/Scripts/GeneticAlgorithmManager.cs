using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class GeneticAlgorithmManager : MonoBehaviour {
    //*C# NOTES*
    //public means that a variable, field or method is availabe outside of this class
    //private means that a variable, field, or method is only available within this class
    //static is essentially a global variable (non-static is used for editing variables within Unity)

    public static GeneticAlgorithmManager Instance { get; set; } //Global instance

    //Genetic Algorithm Constants
    public const float WATCHABLE_RATE = 0.03f;  //~30 frames per second
    public const float TRAINING_RATE = 0.006f; //10,000 frames per second

    //These values are used in both the Generic and Incremental Algorithms
    [Header("General GA Properties")]
    public int _currentGeneration = 0;      //Keeps track of the current generation the system is on
    public int populationSize = 20;         //Population size for each generation
    public int survivors = 10;              //amount of survivors that will be carried on from the previous generation

    //Below is the static properties (aka global variables) to get the values from any part in the code
    public static int currentGeneration {
        get { return Instance._currentGeneration; }
    }
    
    private static bool _lookingForOptimal = false;  //Flag that flips when the first agent passes the finish line
    private static int _lastFitnessIncreaseHappenedOn = -1;    //keeps track of the last generation to have a fitness increase

    [Header("Incremental GA Properties")]
    public bool useIncrementalGA = false;   //determines if the GA uses incremental or 'normal' GA at the start
    public int frameLimit = 30;             //the current limit of frames (at the start acts as the initial frame allowance
    public int everyXGenerations = 20;      //How often the frame limit is increased (eg. frameLimit += frameIncrease every 20 generations)
    public int frameIncrease = 10;          //How much the frame limit is increased by


    //Below is the static properties (aka global variables) to get the values from any part in the code
    //Returns the current frame limit, if incremental is enabled it returns the 'frameLimit' variable if not it returns int.MaxValue
    public static int incrementFrameLimit {
        get { return Instance.useIncrementalGA ? Instance.frameLimit : int.MaxValue; }
        set { if(Instance.useIncrementalGA) Instance.frameLimit = value; }
    }
    public static int incrementEveryXGenerations { get { return Instance.everyXGenerations; } } 
    public static int incrementFrameIncease { get { return Instance.frameIncrease; } }   

    [Header("Termination Conditions")]
    public int generationDeadline = 10; //How many generations the genetic algorithm has to improve the fitness score before terminating


    // ------------------ MISC Variables -----------------
    //*Note* [HideInInspector] is just used to disable these variables from showing up in Unity to be altered via the Interface
    [HideInInspector]
    public bool training = true; //Whether we are training or watching the agents (controls the frame rate)
    [HideInInspector]
    public float _deltaTime = 0.09f; //time step between frames (used to keep the step constant between 10,000 and 30 frames per second)
    public static Stopwatch stopwatch = new Stopwatch(); //Used to keep track of the total calculation time
    [NonSerialized]
    public PrintLog log;    //used to log the DATA / RESULTS of the genetic algorithm
                            //Records Current Generation, Best Score, Average Score, Worst Score
    [NonSerialized]
    public static bool showingBestRun = false;  //Isolates the best possible agent run (stops the algorithm)

    private static Agent _veryBestAgentEver = null;
    public static Agent veryBestAgentEver {
        get { return _veryBestAgentEver; }
        set {
            _veryBestAgentEver = value;
            Instance.veryBestFitnessScore = value.fitness;
            _lastFitnessIncreaseHappenedOn = currentGeneration;
        }
    }
    public float veryBestFitnessScore = -1;

    //--------------- OBJECT REFERENCES -----------------
    public GameObject agentPrefab;   //Just a reference to the agent's prefab (a game object that is Instantiated X (population size) times)
    [HideInInspector]
    public Transform _finishLine;   //Reference to the finish line (checking if an agent made it pass this)
    [HideInInspector]
    public UnityEngine.UI.Text textField; //Reference to the generation counter UI text
    [HideInInspector]
    public GameObject completeLabel; //Reference to the complete Label to be shown when GA is done

    public UnityEngine.UI.Text countdownField;

    #region STATIC VARIABLES
    // Agents that are still running / performing
    public static HashSet<Agent> activeAgents = new HashSet<Agent>();
    // All Agents for the current generation
    public static List<Agent> generationAgents = new List<Agent>();

    public static List<AgentInterface> interfaces;
    public static float deltaTime {
        get { return Instance._deltaTime; }
    }

    public static float finishLine {
        get { return 99; }
    }

    public static int frameCount = 0;//the frame cur gen is on 
    public static int PopulationSize {
        get { return Instance.populationSize; }
    }
    #endregion

    //Registers this Manager on creation as the only instance of itself (so that we can gather non-static variables)
    public GeneticAlgorithmManager() {
        Instance = this;
    }

    //When the game starts
    void Awake() {
        log = new PrintLog(0); //makes a new log
        log.SetHeader("Generation\tBest\tAvg\tWorst"); //sets the header

        //Due to how the code operates, population size must be even (displays a warning if it isnt)
        if (populationSize % 2 != 0) {
            Debug.LogError("Population size has to be even!");
        }

        Time.fixedDeltaTime = training ? TRAINING_RATE : WATCHABLE_RATE;    //Sets the frame rate
        textField.text = "0";   //Sets the generation count display to 0

        //Creates the agents
        interfaces = new List<AgentInterface>(populationSize);
        for(int i = 0; i < populationSize; i++) {
            var gameObj = Instantiate<GameObject>(agentPrefab);
            gameObj.name = "Agent " + i.ToString();
            gameObj.transform.position = this.transform.position;
            var agentInterface = gameObj.GetComponent<AgentInterface>();
            interfaces.Add(agentInterface);
            if(i < 10) {
                var renderer = gameObj.GetComponentInChildren<SpriteRenderer>();
                renderer.color = new Color(0.8f, 0.3f, 0.3f, 1f);
                renderer.sortingOrder = 100;
            }
        }

        StartCoroutine(UpdateText());   //Starts a function that is called every 1 second to update the generation text
    }

    //Updates in a loop forever to update the generation count UI
    public IEnumerator UpdateText() {
        while(true) {
            yield return new WaitForSeconds(1); //waits 1 second
            textField.text = _currentGeneration.ToString(); //converts the int to a string
        }
    }

    //keeps track if the frame count should be increased (disabled during a new generation)
    private bool _creatingNewGeneration = true; 
    
    //Called every single frame
    void FixedUpdate() {
        if(_creatingNewGeneration) return;
        frameCount++;
    }

    void Start() {
        StartCoroutine(Countdown());
    }

    public void ToggleFramerate() {
        if (training) {
            training = false;
            Time.fixedDeltaTime = WATCHABLE_RATE;
        }
        else {
            training = true;
            Time.fixedDeltaTime = TRAINING_RATE;
        }
    }

    public IEnumerator Countdown() {
        countdownField.text = 3.ToString();
        yield return new WaitForSecondsRealtime(1);

        countdownField.text = 2.ToString();
        yield return new WaitForSecondsRealtime(1);

        countdownField.text = 1.ToString();
        yield return new WaitForSecondsRealtime(1);

        countdownField.gameObject.SetActive(false);

        GeneticAlgorithmManager.showingBestRun = false;
        StartNewGeneration();
    }

    //Start of training
    public void StartNewGeneration() {
        _creatingNewGeneration = true;

        _currentGeneration = 1;
        //For every agent interface, it creates a brand new randomly generated agent
        foreach(var inter in interfaces) {
            inter.NewGenerateAgent();
        }

        _creatingNewGeneration = false;
        stopwatch.Start();  //Starts the calculation timer
    }

    public IEnumerator ShowBestOnly() {
        generationAgents.Clear();
        activeAgents.Clear();
        frameCount = 0;

        yield return new WaitForFixedUpdate(); //wait one frame

        interfaces[0].LoadAgent(veryBestAgentEver);
        interfaces[0].transform.position = Instance.transform.position;
        interfaces[0].gameObject.SetActive(true);
    }
    
    /// <summary>
    /// On Generation run completion, this is called
    /// </summary>
    public static void GenerationComplete() {
        //If the generation completed and the deadline was met. End the training program
        if (_lastFitnessIncreaseHappenedOn > 0 && (currentGeneration - _lastFitnessIncreaseHappenedOn) > Instance.generationDeadline) {
            Debug.Log(stopwatch.Elapsed);   //Display the time it took
            Instance.completeLabel.SetActive(true); //show the COMPLETE label

            //Displays only the best agent to run
            showingBestRun = true; 
            Instance.StartCoroutine(Instance.ShowBestOnly());

            //If was in the 'training' mode speed reset to a watchable speed
            if(Instance.training) {
                Instance.training = false;
                Time.fixedDeltaTime = WATCHABLE_RATE;
            }

            return;
        }

        //If this was called but there are no agents, end
        if(generationAgents.Count == 0) return;

        float totalFitness = 0;
        //Sorts the agents from best to worst fitness score
        List<Agent> sortedAgents = SortByFitness(out totalFitness); 
        //Creates half population size list of parent pairs based on fitness propability
        List<Agent[]> parentPairs = GetParentPairs(sortedAgents, totalFitness);

        //Clears the old generational list
        generationAgents.Clear();
        //Ensure that active agent list is empty
        activeAgents.Clear();

        //enable new generation flag
        Instance._creatingNewGeneration = true;
        frameCount = 0; //reset frame count

        List<Agent> allOffspring = new List<Agent>();
        //add all survivors (top X agents from previous generation)
        for (int i = 0; i < Instance.survivors; i++) {
            Agent survivor = sortedAgents[i];
            survivor.Reset();
            allOffspring.Add(survivor);
        }

        //Create all of the offsprings to fill up the remaining population using the parent pairs from above
        for(int i = 0; i < parentPairs.Count - (Instance.survivors/2); i++) {
            Agent[] offspring = Crossover(parentPairs[i]);
            allOffspring.Add(offspring[0]);
            allOffspring.Add(offspring[1]);
        }

        //If current generation is a mod of the incrementEvery.. increase the frame limit for incremental
        if (GeneticAlgorithmManager.currentGeneration % incrementEveryXGenerations == 0 ){
            incrementFrameLimit += incrementFrameIncease;
        } 

        //Loads up the newly created agents into their interfaces (object pooling)
        Instance.StartCoroutine(Instance.StartNextGeneration(allOffspring.ToArray()));
    }

    /// <summary>
    /// Reorganizes the agent list into a list sorted by fitness score. Element 0 = best fitness, last element = worst fitness.
    /// This function will also 'recalculate' the fitness score for all the agents and gather the sum of all the fitness scores to be used
    /// in the parent pairing function.
    /// </summary>
    /// <param name="totalFitness"></param>
    /// <returns></returns>
    private static List<Agent> SortByFitness(out float totalFitness) {
        List<Agent> sortedAgents = new List<Agent>(generationAgents.Count);
        sortedAgents.Add(generationAgents[0]);
        float summedFitness = generationAgents[0].fitness;

        for(int i = 1; i < generationAgents.Count; i++) {
            bool placed = false;

            for(int k = 0; k < sortedAgents.Count; k++) {
                if(generationAgents[i].fitness > sortedAgents[k].fitness) {
                    placed = true;
                    sortedAgents.Insert(k, generationAgents[i]);
                    break;
                }
            }


            if(!placed)
                sortedAgents.Add(generationAgents[i]);

            summedFitness += generationAgents[i].fitness;
        }

        if(_lookingForOptimal) {
            if(Instance.veryBestFitnessScore < sortedAgents[0].fitness) {
                veryBestAgentEver = sortedAgents[0];
            }
        }
        else if(sortedAgents[0].finishedTrack) {
            _lookingForOptimal = true;
            veryBestAgentEver = sortedAgents[0];
        }

        Instance.log.AddLine(currentGeneration + "\t" + sortedAgents[0].fitness + "\t" + (summedFitness/generationAgents.Count) + "\t" + sortedAgents[sortedAgents.Count-1].fitness);

        int count = 0;
        foreach(Agent agent in sortedAgents) {
            if((agent.fitness - sortedAgents[sortedAgents.Count - 1].fitness) > 0) {
                count++;
                if(count > 1)
                    break;
            }
        }

        if(count > 1) {
            summedFitness -= (sortedAgents[sortedAgents.Count - 1].fitness * sortedAgents.Count);
            foreach(Agent agent in sortedAgents) {

                agent.fitness -= sortedAgents[sortedAgents.Count - 1].fitness;
            }
        }




        totalFitness = summedFitness;
        //for (int i = 0; i < sortedAgents.Count; i++) {
        //    Debug.Log(sortedAgents[i].fitness);
        //}

        return sortedAgents;
    }

    /// <summary>
    /// Given the list of sorted agents this function will create a list of (populationSize/2) parent pairs.
    /// Each agent array in the list will consist of two agents (mom and dad).
    /// See report for more details
    /// </summary>
    /// <param name="sortedAgents"></param>
    /// <param name="totalFitnessScore"></param>
    /// <returns></returns>
    public static List<Agent[]> GetParentPairs(List<Agent> sortedAgents, float totalFitnessScore) {
        List<Agent[]> allParents = new List<Agent[]>();

        for(int i = 0; i < Instance.populationSize/2; i++) {
            Agent[] parents = new Agent[2]; //array parents will hold 2 parent agents

            //select first parent
            float targetValue = Random.Range(0, totalFitnessScore);
            float fitnessCount = 0;

            foreach(Agent agent in sortedAgents) {
                float currentTarget = targetValue - fitnessCount;
                if(currentTarget > 0 && currentTarget <= agent.fitness) {
                    parents[0] = agent;
                    break;
                }
                fitnessCount += agent.fitness;
            }

            if(parents[0] == null)
                Debug.Log("Dad is null : " + targetValue + " : " + fitnessCount);

            //select second parent
            float targetValue2 = Random.Range(0, totalFitnessScore);
            fitnessCount = 0;

            Agent previousAgent = null;
            foreach(Agent agent in sortedAgents) {
                float currentTarget = targetValue2 - fitnessCount;
                if(currentTarget > 0 && currentTarget <= agent.fitness) {
                    if(agent == parents[0]) {
                        if(previousAgent != null) {
                            parents[1] = previousAgent;
                            break;
                        }

                        continue;
                    }

                    parents[1] = agent;
                    break;
                }

                previousAgent = agent;
                fitnessCount += agent.fitness;
            }

            if(parents[1] == null)
                Debug.Log("Mom is null : " + targetValue + " : " + fitnessCount);

            allParents.Add(parents);
        }

        return allParents;
    }

    /// <summary>
    /// Given two parent agents, this function will produce two new agents by crossing over the chromosomes from each parent.
    /// Half goes into one child and the other half goes into the other child (see report for more details)
    /// </summary>
    /// <param name="parents">Parents must be an array consisting of 2 parents, a mom and dad</param>
    /// <returns></returns>
    public static Agent[] Crossover(Agent[] parents) {
        if(parents[0] == null || parents[1] == null)
            Debug.Log((parents[0] == null) + " : " + (parents[1] == null));

        // split dad and mom into three parts at two random indeces
        // D1, D2, D3 and M1, M2, M3
        // Child 1 gets D1, M2, D3
        // Child 2 gets M1, D2, M3
        Agent[] children = new Agent[2]; //array children will hold 2 child agents
        children[0] = new Agent();
        children[1] = new Agent();

        int firstIndex = Random.Range(0, Agent.CHROMOSOME_SIZE); //0 to 99
        int lastIndex = Random.Range(0, Agent.CHROMOSOME_SIZE); //0 to 99

        if(firstIndex > lastIndex) { //Swap them
            int temp = firstIndex;
            firstIndex = lastIndex;
            lastIndex = firstIndex;
        }

        for(int i = 0; i < Agent.CHROMOSOME_SIZE; i++) {
            if(i >= firstIndex && i < lastIndex) {
                children[0].chromosome[i] = parents[1].chromosome[i]; //Child 1 gets M2
                children[1].chromosome[i] = parents[0].chromosome[i]; //Child 2 gets D2
            }
            else {
                children[0].chromosome[i] = parents[0].chromosome[i]; //Child 1 gets D1 and D3
                children[1].chromosome[i] = parents[1].chromosome[i]; //Child 2 gets M1 and M3
            }
        }


        //5, 0.03
        children[0].chromosome.Mutate(5, 0.30f);
        children[1].chromosome.Mutate(5, 0.30f);

        return children;
    }

    /// <summary>
    /// Call this to begin the next generation by supplying an array of new agents to be tested
    /// </summary>
    /// <param name="offspringAgents"></param>
    /// <returns></returns>
    public IEnumerator StartNextGeneration(Agent[] offspringAgents) {
        yield return new WaitForFixedUpdate(); //wait one frame
        
        for(int i = 0; i < offspringAgents.Length; i++) {
            interfaces[i].LoadAgent(offspringAgents[i]);                        //Loads the agent into the interface
            interfaces[i].transform.position = Instance.transform.position;     //Resets the interface's position
            interfaces[i].gameObject.SetActive(true);                           //Enables the interface
        }

        _currentGeneration++;   //Increment the generation counter

        _creatingNewGeneration = false;
    }

    /// <summary>
    /// This is called whenever an agent finishes
    /// </summary>
    /// <param name="agent"></param>
    public static void AgentFinished(Agent agent) {
        if (showingBestRun) {
            if(Math.Abs(Time.fixedDeltaTime - WATCHABLE_RATE) < 0.0001f)
                Time.fixedDeltaTime = WATCHABLE_RATE;
            Instance.StartCoroutine(Instance.ShowBestOnly());
            return;
        }
        
        //remove the agent from the active agent list
        if(activeAgents.Contains(agent)) {
            activeAgents.Remove(agent);
        }

        //if all agents are complete, set generation as complete!
        if(activeAgents.Count <= 0) {
            GenerationComplete();
        }
    }
}