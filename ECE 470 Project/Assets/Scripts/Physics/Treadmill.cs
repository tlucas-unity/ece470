/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treadmill : MonoBehaviour {
    public float speed;

    private void OnCollisionEnter2D(Collision2D collision) {
        CustomRigidbody crb = collision.gameObject.GetComponent<CustomRigidbody>();
        if(crb) {
            crb.ApplyTreadmill(this);
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        CustomRigidbody crb = collision.gameObject.GetComponent<CustomRigidbody>();
        if(crb) {
            crb.RemoveTreadmill(this);
        }
    }
}