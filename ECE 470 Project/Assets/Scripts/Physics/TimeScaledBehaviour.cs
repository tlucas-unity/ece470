/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TimeScaledBehaviour : MonoBehaviour {
    public abstract float timeScale { get; }
    public abstract void AddTimeField(TimeField field);
    public abstract void RemoveTimeField(TimeField field);
}
