using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaledObject : TimeScaledBehaviour {

    [Range(0.1f, 5)]
    public float timeDistortion = 1;

    public override float timeScale { get { return timeDistortion; } }

    public override void AddTimeField(TimeField field) {
        throw new NotImplementedException();
    }
    public override void RemoveTimeField(TimeField field) {
        throw new NotImplementedException();
    }
}
