/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class TimeField : MonoBehaviour {

    [Tooltip("Distortion is multipled by Time Scale, 0.5 = half as fast, 1 = normal, 2 = twice as fast")]
    [SerializeField]
    private float distortion = 0.5f;
    [SerializeField]
    private Collider2D _collider;

    public float Distortion { get { return distortion; } }
    public Collider2D Collider { get { return _collider; } }

    protected virtual void Reset() {
        GetComponent<Collider2D>().isTrigger = true;
        this.gameObject.layer = LayerMask.NameToLayer("Time Field");
        _collider = GetComponent<Collider2D>();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision) {
        CustomRigidbody _rigidbody = collision.gameObject.GetComponent<CustomRigidbody>();
        if(_rigidbody) 
            _rigidbody.AddTimeField(this);        
    }

    protected virtual void OnTriggerExit2D(Collider2D collision) {
        CustomRigidbody _rigidbody = collision.gameObject.GetComponent<CustomRigidbody>();
        if(_rigidbody)
            _rigidbody.RemoveTimeField(this);
    }
}
