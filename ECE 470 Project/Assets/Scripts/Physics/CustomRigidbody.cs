/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

[System.Flags]
public enum Direction { None = 0, Up = 1, Left = 2, Right = 4, Down = 8, LeftOrRight = 6, UpOrDown = 9 }

[SelectionBase]
public class CustomRigidbody : TimeScaledBehaviour {

    public const float skinWidth = 0.05f;
    //private const float BOUNCE_THRESHOLD = 7.5f;   //Magnitude of reflected velocity has to be greater than threshold

    public Transform transformOverride = null;
    public Transform TargetTransform { get { if(!transformOverride) return transform; return transformOverride; } }

    [Header("Physics")]
    [Tooltip("0 or negative mass is considered to be infinite mass, forces will not apply")]
    public float mass = 1;    
    [Range(0, 1)][Tooltip("Effects of environmental drag -> Drag = 1, 100% drag")]
    public float drag = 1;
    [Range(0, 2)]
    public float friction = 1;
    public float timeDistortion = 1;
    //[Range(0, 1)][Tooltip("Surface bounce * Rigidbody bounce = energy reflected")]
    //public float bounce = 1;
    [Tooltip("Resistance to Time Distortion")][Range(0, 1)]
    public float timeResistance = 0;
    /// <summary>
    /// Returns the time scale after time distortion and time resistance for the object
    /// </summary>
    public override float timeScale { get { return timeDistortion * velocity.TimeScale; } }

    [Header("Gravity")]
    public float gravityScale = 1;
    [Tooltip("Multiplier on gravity when the object is falling (eg. free falling)")]
    public float fallMultiplier = 1;
    [Tooltip("Multiplier on gravity when the object is ascending (eg. jumping)")]
    public float ascendMultiplier = 1;

    [Header("Ray-casting")]
    [Tooltip("If left empty, Rigidbody will ignore collision")]
    public MultistateCollider multistateCollider;
    [Delayed][Range(0.01f, 0.3f)]
    public float maximumRayGap = 0.08f;
    [Range(0.01f, 2.0f)]
    public float rayOverscan = 0.1f;

    [Header("Properties")]
    [Range(0, 89.9f)]
    public float maxClimbAngle = 80f;
    [Tooltip("[REQUIRES KINEMATIC RIGIDBODY2D] Determines if the Custom Rigidbody will interact properly with normal 2D Rigidbodies (Applying forces when collision occurs)")]
    public bool interactivePhysics = false;
    [Tooltip("[REQUIRES INTERACTIVE PHYSICS] Simulates a random +Y force as if the object was being kicked by feet while running")]
    public bool simulateFeet = false;
    public bool relativeVelocity = true;
    
    /// <summary>
    /// Normal Velocity of the Rigidbody (Does not include forces or external velocity (treadmill + moving platform))
    /// </summary>
    public DampingVelocity velocity;
    
    [Header("Collision Masks")]
    [Tooltip("Interacts with this mask (does not use default Physic Layer Interaction)")]
    public LayerMask environmentMask;
    public LayerMask movingEnvironmentMask;

#if UNITY_EDITOR
    [Header("Debug Tools")]
    [SerializeField]
    private bool drawRays = false;
    [ReadOnly][SerializeField]
#endif
    CollisionInfo collisionInfo;

    /*                          Internal Variables                                */
    LayerMask collisionMask;   //used internally for actually raycasts, compares against enironmentMask | movingMask 
    static LayerMask interactiveEnvironmentMask;// = Common.INTERACTIVE_PHYSICS_MASK; //environment physics layer  

    List<TimeField> timeFields = new List<TimeField>();
    Treadmill activeTreadmill;
    PhysicsMaterial2D surface;
    float environmentDensity = 0;   //Air has a density of 0
    //float timeDistortion = 0;       //0 = normal, 0.5 slower, 2 faster
    //float personalTimeScale { get { return 1 + (timeDistortion*(1-timeResistance)); } }

    RaycastStructure raycastStruct;
    Vector2 _frameVelocity; //Used internally by collisions (frameVelocity pre-Collisions)
    Vector2 movingPlatformFrameVelocity;

    /// <summary>
    /// Returns the true velocity of the object through world space (includes external velocity)
    /// </summary>
    public Vector2 GlobalVelocity {
        get {
            Vector2 personalVelocity = (relativeVelocity) ? (Quaternion.Inverse(TargetTransform.rotation)*Velocity).ToVector2() : Velocity;
            Vector2 externalVelocity = (movingPlatformFrameVelocity / Time.deltaTime);

            return personalVelocity + externalVelocity;
        }
        set {
            throw new NotImplementedException();
            Debug.LogError("NYI");
            Vector2 unwrappedVelocity = (relativeVelocity) ? (TargetTransform.rotation*value).ToVector2() : value;
            unwrappedVelocity -= movingPlatformFrameVelocity/Time.deltaTime;
            velocity.target = unwrappedVelocity;
        }
    } 
    /// <summary>
    /// Returns the velocity of the object ignoring any velocity applied to it externally
    /// </summary>
    public Vector2 Velocity {
        get { return velocity.vector; }
    }

    public delegate void PostCollision(CollisionInfo info);
    public event PostCollision postCollision;

    public delegate void PostMovement(CollisionInfo info, Vector2 frameVelocity);
    public event PostMovement postMovement;

    void Reset() {
        multistateCollider = GetComponent<MultistateCollider>();
        environmentMask = 1<<LayerMask.NameToLayer("Environment");
        movingEnvironmentMask = 1<<LayerMask.NameToLayer("Moving Environment");
    }

    private void Awake() {
        collisionMask = environmentMask | movingEnvironmentMask;
#if UNITY_EDITOR
        if(interactivePhysics) {
            if(!GetComponent<Rigidbody2D>())
                Debug.LogWarning(this.gameObject + " has interactive physics enabled but no rigidbody 2D");
        }
#endif
    }

    public void DoWork() {
        if (Time.timeScale < 0.001f || timeDistortion < 0.001f) return;

        ApplyGravity();
        
        velocity.TimeScale = 1/((1 - timeResistance) + (timeResistance*timeDistortion));
        float surfaceFriction = (surface) ? surface.friction : 1;
        velocity.Update(friction*surfaceFriction, drag*environmentDensity);
        
        Vector2 frameVelocity = velocity.vector * GeneticAlgorithmManager.deltaTime * timeDistortion;

        if (frameVelocity.x > 1) {
            Debug.Log(frameVelocity);
        }

        if(!relativeVelocity)
            frameVelocity = Quaternion.Inverse(TargetTransform.rotation)*frameVelocity;
                
        //Collision Check against a single frame of velocity
        if(collisionMask != 0 && multistateCollider) {   //Ignore the check if object cannot hit anything
            Collisions(ref frameVelocity);//, totalVelocity);
            
            if(collisionInfo.left || collisionInfo.right) {
                velocity.target.x = 0;
                velocity.SetX(0);
            }

            if(collisionInfo.below || collisionInfo.above) {
                velocity.target.y = 0; //Reset Gravity
                velocity.SetY(0);
            }

            //TODO: Implement Bounce
            //if(surface && (surface.bounciness*bounce) > 0) {
            //    float bounciness = (surface.bounciness*bounce);
            //    Vector2 reflectVelocity = (_frameVelocity / frameTime);
            //    float angle = Vector2.Angle(collisionInfo.hitNormal, reflectVelocity);

            //    reflectVelocity *= bounciness;

            //    if(reflectVelocity.magnitude > BOUNCE_THRESHOLD) {
            //        reflectVelocity.x *= Mathf.Cos(angle);
            //        reflectVelocity.y *= Mathf.Sin(angle);
            //        velocity.Set(reflectVelocity);
            //    }
            //}

            if (collisionInfo.activeFrame && postCollision != null)
                postCollision(collisionInfo);
        }
        
        //Move object by the frame velocity after collisions
        TargetTransform.Translate(frameVelocity);

        //Post movement event
        if (postMovement != null)
            postMovement(collisionInfo, frameVelocity);

        if(collisionInfo.movingPlatform) {
            collisionInfo.movingPlatformPosition = collisionInfo.movingPlatform.transform.position;
        }
    }
    void LateUpdate() {
        if(collisionInfo.movingPlatform //If last frame there was a movingPlatform
        && collisionInfo.movingPlatform.transform.position.ToVector2() != collisionInfo.movingPlatformPosition) { //current position != last known position
            Vector2 deltaPosition = collisionInfo.movingPlatform.transform.position.ToVector2() - collisionInfo.movingPlatformPosition;
            movingPlatformFrameVelocity = deltaPosition;
            TargetTransform.Translate(deltaPosition); //Move Player to match
        }
        else {
            movingPlatformFrameVelocity = Vector2.zero;
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if(!interactivePhysics) return;
        if(interactiveEnvironmentMask.Contains(collision.collider) && GlobalVelocity != Vector2.zero) {
            Vector2 force = (GlobalVelocity * Time.deltaTime * mass);
            if(simulateFeet && force.y >= 0)
                force.y += Random.value * mass * Time.deltaTime;
                
            collision.rigidbody.AddForceAtPosition(force, collision.contacts[0].point, ForceMode2D.Impulse);
        }
    }

    /// <summary>
    /// Scans in all directions [0] == down and left, [1] == up and right for return
    /// </summary>
    public CollisionInfo Scan(float scanDistance) {        
        return Cast((Direction.LeftOrRight | Direction.UpOrDown), scanDistance);
    }
    public CollisionInfo Cast(Direction direction, float distance = -1) {
        if(distance < 0)
            distance = rayOverscan;
        RaycastStructure raycastStruct = new RaycastStructure(multistateCollider, maximumRayGap);
        CollisionInfo returnValue = new CollisionInfo();

        float rayLength = skinWidth + distance;
        Vector2 rayOffsetDirection = (TargetTransform.rotation*Vector2.up).normalized;

        if((direction & Direction.Left) > 0){
            //Left
            Vector2 rayDirection = (TargetTransform.rotation*Vector2.left).normalized;
            for(int i = 0; i < raycastStruct.horizontalRayCount; i++) {
                Vector2 rayOrigin = raycastStruct.bottomLeft;
                rayOrigin += rayOffsetDirection * (raycastStruct.horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, rayDirection, rayLength, collisionMask);

                if(hit) {
                    returnValue.horizontalRayMask += 1 << i;
                    returnValue.left = true;
                }
            }
        }
        if((direction & Direction.Right) > 0) {
            //Right
            Vector2 rayDirection = (TargetTransform.rotation*Vector2.right).normalized;
            for(int i = 0; i < raycastStruct.horizontalRayCount; i++) {
                Vector2 rayOrigin = raycastStruct.bottomRight;
                rayOrigin += rayOffsetDirection * (raycastStruct.horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, rayDirection, rayLength, collisionMask);

                if(hit) {
                    returnValue.horizontalRayMask += 1 << i;
                    returnValue.right = true;
                }
            }
        }
        
        rayOffsetDirection = (TargetTransform.rotation*Vector2.right).normalized;
        if((direction & Direction.Up) > 0) {
            //Up
            Vector2 rayDirection = (TargetTransform.rotation*Vector2.up).normalized;
            for(int i = 0; i < raycastStruct.verticalRayCount; i++) {
                Vector2 rayOrigin = raycastStruct.topLeft;
                rayOrigin += rayOffsetDirection * (raycastStruct.verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, rayDirection, rayLength, collisionMask);

                if(hit) {
                    returnValue.verticalRayMask += 1 << i;
                    returnValue.above = true;
                }
            }
        }
        if((direction & Direction.Down) > 0) {
            //Down
            Vector2 rayDirection = (TargetTransform.rotation*Vector2.down).normalized;
            for(int i = 0; i < raycastStruct.verticalRayCount; i++) {
                Vector2 rayOrigin = raycastStruct.bottomLeft;
                rayOrigin += rayOffsetDirection * (raycastStruct.verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, rayDirection, rayLength, collisionMask);

                if(hit) {
                    returnValue.verticalRayMask += 1 << i;
                    returnValue.below = true;
                }
            }
        }

        returnValue.verticalRayCount = raycastStruct.verticalRayCount;
        returnValue.horizontalRayCount = raycastStruct.horizontalRayCount;
        return returnValue;
    }
    public bool OverlapCast(string stateName) {
        if(multistateCollider.states[multistateCollider.CurrentState].name == stateName) return false;

        return OverlapCast(multistateCollider.states.FindIndex(x => x.name == stateName));        
    }
    public bool OverlapCast(int index = 0) {
        if(index == multistateCollider.CurrentState) return false;

        ColliderData data = multistateCollider.states[index];
        Rect collider = new Rect(data.offset, data.size/2);
        collider.center += TargetTransform.position.ToVector2();
        
        return (Physics2D.OverlapBox(collider.center, collider.size, 0, collisionMask));
    }
    
    public void SetVelocityX(float xVelocity) {
        velocity.SetX(xVelocity);
    }
    public void SetVelocityY(float yVelocity) {
        velocity.SetY(yVelocity);
    }
    public void SetVelocity(Vector2 velocity) {
        this.velocity.Set(velocity);
    }

    public void ApplyImpulseForce(Vector2 force, bool ignoreMass = false) {
        velocity.Add(ForceToVelocity(force, ignoreMass));
    }
    private Vector2 ForceToVelocity(Vector2 force, bool ignoreMass = false) {
        if(ignoreMass)
            return force;
        else
            return (force / mass);
    }

    public override void AddTimeField(TimeField timeField) {
        if(timeFields.Contains(timeField)) return;
        timeFields.Add(timeField);
    }
    public override void RemoveTimeField(TimeField timeField) {
        timeFields.Remove(timeField);
    }
    public void ApplyTreadmill(Treadmill treadmill) {
        if(activeTreadmill != null) {
            velocity.AddOffsetX(-activeTreadmill.speed, true);
        }
        activeTreadmill = treadmill;
        velocity.AddOffsetX(treadmill.speed, true);
    }
    public void RemoveTreadmill(Treadmill treadmill) {
        if(activeTreadmill != treadmill) return;
        velocity.AddOffsetX(-activeTreadmill.speed, true);
        activeTreadmill = null;   
    }
    public void SetEnvironmentDensity(float density) {
        environmentDensity = density;
    }
    
    void Collisions(ref Vector2 frameVelocity) {
            collisionInfo.Reset();
        _frameVelocity = frameVelocity;
        raycastStruct = new RaycastStructure(multistateCollider, maximumRayGap);

        if (frameVelocity.x != 0) {
            collisionInfo.activeFrame = true;
            float directionX = Mathf.Sign (frameVelocity.x);
            float rayLength = (rayOverscan > Mathf.Abs (frameVelocity.x))?skinWidth + rayOverscan : skinWidth + Mathf.Abs (frameVelocity.x);
            Vector2 rayDirection = (TargetTransform.rotation*Vector2.right).normalized;
            Vector2 rayOffsetDirection = (TargetTransform.rotation*Vector2.up).normalized;
            collisionInfo.horizontalRayCount = raycastStruct.horizontalRayCount;

            for (int i = 0; i < raycastStruct.horizontalRayCount; i++) {
                Vector2 rayOrigin = (directionX < 0) ? raycastStruct.bottomLeft : raycastStruct.bottomRight;
                rayOrigin += rayOffsetDirection * (raycastStruct.horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, rayDirection * directionX, rayLength, collisionMask);

#if UNITY_EDITOR
                if(drawRays)
                    Debug.DrawRay(rayOrigin, rayDirection * directionX * rayLength, Color.blue);
#endif

                if (hit) {
                    collisionInfo.horizontalRayMask += 1 << i;
                    collisionInfo.collision = hit.collider;
                    //collisionInfo.hitNormal = hit.normal;

                    if (hit.distance < (Mathf.Abs(frameVelocity.x) + skinWidth)) {
                        float slopeAngle = Vector2.Angle(hit.normal, (TargetTransform.rotation*Vector2.up).normalized);

                        if (i == 0 && slopeAngle <= maxClimbAngle) {
                            if (collisionInfo.descendingSlope) {
                                collisionInfo.descendingSlope = false;
                                frameVelocity = _frameVelocity;
                            }

                            //IF SLOPE CHANGES
                            float distanceToSlopeStart = 0;
                            if (slopeAngle != collisionInfo.slopeAngleOld) {
                                distanceToSlopeStart = hit.distance-skinWidth;
                                frameVelocity.x -= distanceToSlopeStart * directionX;
                            }
                            AscendingSlope(ref frameVelocity, slopeAngle);
                            frameVelocity.x += distanceToSlopeStart * directionX;

                            if(movingEnvironmentMask.Contains(hit.collider))
                                collisionInfo.movingPlatform = hit.collider;

                            //***************** CHECK TO SEE IF I"M GOING OVER THE HILL *******************************//
                            Vector2 rotatedFrameVelo = new Vector2(0, frameVelocity.y);
                            rotatedFrameVelo = TargetTransform.rotation*rotatedFrameVelo;
                            rayOrigin += rotatedFrameVelo;

                            RaycastHit2D check = Physics2D.Raycast(rayOrigin, rayDirection * directionX, rayLength, collisionMask);
#if UNITY_EDITOR
                            if(drawRays)
                                Debug.DrawRay(rayOrigin, rayDirection * directionX * rayLength, Color.cyan);
#endif
                            if (!check) {   //if it missed, scan how far over and move the frame velocity
                                rayOrigin += rayDirection * directionX * rayLength;
                                RaycastHit2D down = Physics2D.Raycast(rayOrigin, (TargetTransform.rotation*Vector2.down).normalized, Mathf.Abs(frameVelocity.y), collisionMask);
#if UNITY_EDITOR
                                if(drawRays)
                                    Debug.DrawRay(rayOrigin, (TargetTransform.rotation*Vector2.down).normalized * frameVelocity.y, Color.yellow);
#endif
                                if (down) {
                                    frameVelocity.y -= (down.distance - skinWidth);
                                }
                            }
                        }
                        //else { 
                        if (!collisionInfo.ascendingSlope || slopeAngle > maxClimbAngle) { //Else I hit something but can't climb up it
                            frameVelocity.x = (hit.distance - skinWidth) * directionX;
                            //rayLength = hit.distance;

                            if (collisionInfo.ascendingSlope) {
                                frameVelocity.y = Mathf.Tan(collisionInfo.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(frameVelocity.x);
                            }

                            collisionInfo.left = (directionX < 0);
                            collisionInfo.right = (directionX > 0);
                        }
                    }
                    else {
                        //rayLength = hit.distance;
                        float newDistance = hit.distance - (Mathf.Abs(frameVelocity.x) + skinWidth);
                        if (newDistance < collisionInfo.overscan.x)
                            collisionInfo.overscan.x = newDistance;
                        collisionInfo.leftOverscan = (directionX < 0);
                        collisionInfo.rightOverscan = (directionX > 0);
                    }
                }
            }
        }
        else {
            collisionInfo.direction.x = 0;
        }

        if (frameVelocity.y != 0) {
            collisionInfo.activeFrame = true;
            float directionY = Mathf.Sign (frameVelocity.y);
            float rayLength = (rayOverscan > Mathf.Abs (frameVelocity.y))?skinWidth + rayOverscan:skinWidth+Mathf.Abs (frameVelocity.y);
            Vector2 rayDirection = (TargetTransform.rotation*Vector2.up).normalized;
            Vector2 rayOffsetDirection = (TargetTransform.rotation*Vector2.right).normalized;
            collisionInfo.verticalRayCount = raycastStruct.verticalRayCount;

            for (int i = 0; i < raycastStruct.verticalRayCount; i++) {
                Vector2 rayOrigin = (directionY < 0) ? raycastStruct.bottomLeft : raycastStruct.topLeft;
                //Vector2 rayOrigin = raycastStruct.left;
                rayOrigin += rayOffsetDirection * (raycastStruct.verticalRaySpacing * i + frameVelocity.x);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, rayDirection * directionY, rayLength, collisionMask);

#if UNITY_EDITOR
                if(drawRays)
                    Debug.DrawRay(rayOrigin, rayDirection * directionY * rayLength, Color.red);
#endif

                if (hit) {
                    collisionInfo.verticalRayMask += 1 << i;
                    collisionInfo.collision = hit.collider;
                    //collisionInfo.hitNormal = hit.normal;

                    if (hit.distance < (Mathf.Abs(frameVelocity.y) + skinWidth)) {
                        frameVelocity.y = (hit.distance - skinWidth) * directionY;
                        //rayLength = hit.distance;

                        if (collisionInfo.ascendingSlope) {
                            frameVelocity.x = frameVelocity.y / Mathf.Tan(collisionInfo.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(frameVelocity.x);
                        }

                        collisionInfo.below = (directionY < 0);
                        collisionInfo.above = (directionY > 0);

                        if(collisionInfo.below && movingEnvironmentMask.Contains(hit.collider))
                            collisionInfo.movingPlatform = hit.collider;
                    }
                    else {
                        //rayLength = hit.distance;
                        float newDistance = hit.distance - (Mathf.Abs(frameVelocity.y) + skinWidth);
                        if (newDistance < collisionInfo.overscan.y)
                            collisionInfo.overscan.y = newDistance;

                        collisionInfo.belowOverscan = (directionY < 0);
                        collisionInfo.aboveOverscan = (directionY > 0);
                    }
                }                
            }

            if (collisionInfo.ascendingSlope) {
                float directionX = Mathf.Sign(frameVelocity.x);
                rayLength = Mathf.Abs(frameVelocity.x) + skinWidth;
                Vector2 rayOrigin = ((directionX < 0) ? raycastStruct.bottomLeft : raycastStruct.bottomRight) + rayDirection * frameVelocity.y;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, (TargetTransform.rotation*Vector2.right).normalized * directionX, rayLength, collisionMask);

                if (hit) {
                    float slopeAngle = Vector2.Angle(hit.normal, (TargetTransform.rotation*Vector2.up).normalized);
                    if (slopeAngle != collisionInfo.slopeAngle) {
                        frameVelocity.x = (hit.distance - skinWidth) * directionX;
                        collisionInfo.slopeAngle = slopeAngle;
                    }
                }
            }
        }
        
        if (frameVelocity.y <= -0.0001f && frameVelocity.x != 0) {
            DescendSlope(ref frameVelocity);
        }

        collisionInfo.direction = new Vector2(frameVelocity.x.Sign(), frameVelocity.y.Sign());

        surface = (collisionInfo.collision)?collisionInfo.collision.sharedMaterial:null;
        //if(collisionInfo.collision) {
        //    surface = collisionInfo.collision.sharedMaterial;
        //    if(collisionInfo.collision.sharedMaterial != null) {
        //        surfaceFriction = collisionInfo.collision.sharedMaterial.friction;
        //        surfaceBounce = collisionInfo.collision.sharedMaterial.bounciness;
        //    }
            //else {
            //    surfaceFriction = 1;
            //    surfaceBounce = 0;
            //}

            //if(surfaceBounce > 0) {
            //    targetVelocity = impactVelocity * -surfaceBounce;
            //}
        //}
    }
    void AscendingSlope(ref Vector2 frameVelocity, float slopeAngle) {
        float moveDistance = Mathf.Abs (frameVelocity.x);
        float climbVelocityY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;
        
        frameVelocity.y = climbVelocityY;
        frameVelocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(frameVelocity.x);
        collisionInfo.below = true;
        collisionInfo.ascendingSlope = true;
        collisionInfo.slopeAngle = slopeAngle;
    }
    void DescendSlope(ref Vector2 frameVelocity) {
        float directionX = Mathf.Sign (frameVelocity.x);
        float rayLength = Mathf.Atan(maxClimbAngle) * (Mathf.Abs(frameVelocity.x) + 0.2f);

        Vector2 rayDirection = (TargetTransform.rotation*Vector2.down).normalized;
        Vector2 rayOrigin = (directionX < 0) ? raycastStruct.bottomRight : raycastStruct.bottomLeft;

        Vector2 rotatedFrameVelo = new Vector2(frameVelocity.x, 0);
        rotatedFrameVelo = TargetTransform.rotation*rotatedFrameVelo;
        rayOrigin += rotatedFrameVelo;
        RaycastHit2D hit = Physics2D.Raycast (rayOrigin, rayDirection, rayLength, collisionMask);

#if UNITY_EDITOR
        if(drawRays)
            Debug.DrawRay(rayOrigin, rayDirection * rayLength, Color.green);
#endif

        if (hit) {
            float slopeAngle = Vector2.Angle(hit.normal, (TargetTransform.rotation*Vector2.up).normalized);

#if UNITY_EDITOR
            if(drawRays)
                Debug.DrawRay(hit.point, (Quaternion.Inverse(TargetTransform.rotation)*hit.normal), Color.magenta);
#endif

            if (slopeAngle != 0 && slopeAngle <= maxClimbAngle && Mathf.Sign((Quaternion.Inverse(TargetTransform.rotation)*hit.normal).x) == directionX) {
                frameVelocity.y = (hit.distance - skinWidth) * -1;

                collisionInfo.slopeAngle = slopeAngle;
                collisionInfo.descendingSlope = true;
                collisionInfo.below = true;

                if(movingEnvironmentMask.Contains(hit.collider))
                    collisionInfo.movingPlatform = hit.collider;
            }
            else if(collisionInfo.wasDescendingSlope) {
                frameVelocity.y = (hit.distance - skinWidth) * -1;

                collisionInfo.slopeAngle = slopeAngle;
                collisionInfo.below = true;

                if(movingEnvironmentMask.Contains(hit.collider))
                    collisionInfo.movingPlatform = hit.collider;
            }
        }
    }    
    void ApplyGravity() {
        //Apply gravity
        Vector2 gravity = Physics2D.gravity * gravityScale;
        gravity *= (velocity.y > 0) ? ascendMultiplier : fallMultiplier;
        velocity.target += gravity * GeneticAlgorithmManager.deltaTime * timeScale;
    }
    void SetTimeDistortion() {
        if(timeFields.Count == 0) return;
    }

    struct RaycastStructure {
        public Vector2 topLeft, topRight, bottomLeft, bottomRight;
        public int horizontalRayCount, verticalRayCount;
        public float horizontalRaySpacing, verticalRaySpacing;

        public Vector2[] internalPoints;

        public RaycastStructure(MultistateCollider collider, float maxRayGap, bool constructInternalPoints = false) {
            Bounds bounds = new Bounds(collider.offset, collider.size);

            bounds.Expand(skinWidth*-2);
            bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            topLeft = new Vector2(bounds.min.x, bounds.max.y);
            topRight = new Vector2(bounds.max.x, bounds.max.y);

            bottomLeft = collider.transform.rotation*bottomLeft;
            bottomRight = collider.transform.rotation*bottomRight;
            topLeft = collider.transform.rotation*topLeft;
            topRight = collider.transform.rotation*topRight;

            bottomLeft += collider.transform.position.ToVector2();
            bottomRight += collider.transform.position.ToVector2();// + collider.offset;
            topLeft += collider.transform.position.ToVector2();// + collider.offset;
            topRight += collider.transform.position.ToVector2();// + collider.offset;

            verticalRayCount = Mathf.CeilToInt(bounds.size.x / maxRayGap);
            horizontalRayCount = Mathf.CeilToInt(bounds.size.y / maxRayGap);

            horizontalRaySpacing = bounds.size.y / (float)horizontalRayCount;
            verticalRaySpacing = bounds.size.x / (float)verticalRayCount;

            //count the very first ray
            verticalRayCount += 1;  
            horizontalRayCount += 1;

            internalPoints = null;
            if(constructInternalPoints)
                internalPoints = ConstructInternalPoints();
        }

        Vector2[] ConstructInternalPoints() {
            List<Vector2> points = new List<Vector2>();
            for(int x = 0; x < horizontalRayCount; x++) {
                for(int y = 0; y < verticalRayCount; y++) {
                    points.Add(new Vector2(x*horizontalRaySpacing, y*verticalRaySpacing));
                }
            }
            return points.ToArray();
        }
    }
}

[System.Serializable]
public struct CollisionInfo {
    [HideInInspector]
    public bool activeFrame;

    [Header("Collision")]
    public bool above;
    public bool below;
    public bool left, right;    
    public Vector2 direction;

    public bool ascendingSlope;
    public bool descendingSlope;
    public bool wasDescendingSlope;

    //public Vector2 hitNormal; //Used for bounce only
    public float slopeAngle;
    [HideInInspector]
    public float slopeAngleOld;

    public Collider2D collision;
    public Collider2D movingPlatform;
    [HideInInspector]
    public Vector2 movingPlatformPosition;

    [Header("Overscan")]
    public bool aboveOverscan;
    public bool belowOverscan;
    public bool leftOverscan, rightOverscan;
    public Vector2 overscan;

    [Header("Ray-casting")]
    public int verticalRayCount;
    /// <summary>
    /// each bit represents a ray (1 = hit, 0 = missed)
    /// LSB = most bottom
    /// </summary>
    public int verticalRayMask;
    /// <summary>
    /// returns true if any of the rays missed
    /// </summary>
    public bool verticalRaysMissed { get { return (((1 << (verticalRayCount)) - 1) != verticalRayMask); } }

    public int horizontalRayCount;
    /// <summary>
    /// each bit represents a ray (1 = hit, 0 = missed)
    /// LSB = most left
    /// </summary>
    public int horizontalRayMask;
    /// <summary>
    /// returns true if any of the rays missed
    /// </summary>
    public bool horizontalRaysMissed { get { return (((1 << (horizontalRayCount)) - 1) != horizontalRayMask); } }  

    public void Reset() {
        activeFrame = false;

        //hitNormal = Vector2.zero;
        wasDescendingSlope = descendingSlope;
        slopeAngleOld = slopeAngle;
        
        movingPlatform = null;
        collision = null;

        horizontalRayCount = 0;
        verticalRayCount = 0;
        horizontalRayMask = 0;
        verticalRayMask = 0;

        slopeAngle = 0;
        overscan = Vector2.one;
        above = below = false;
        aboveOverscan = belowOverscan = false;
        left = right = false;
        leftOverscan = rightOverscan = false;

        ascendingSlope = false;
        descendingSlope = false;
    }
}

[System.Serializable]
public struct DampingVelocity {
    private const float THRESHOLD = 0.01f;
    private const float DRAG_COEFFICIENT = 0.001f;

    public Vector2 vector { get { return _velocity; } }
    /// <summary>
    /// x component of the velocity
    /// </summary>
    public float x { get { return vector.x; } }
    /// <summary>
    /// y component of the velocity
    /// </summary>
    public float y { get { return vector.y; } }

#if UNITY_EDITOR
    [SerializeField][ReadOnly]
#endif
    private Vector2 _velocity;

    public Vector2 target;  //velocity target

    /// <summary>
    /// Offset will always affect the velocity target, however it can be changed to instantly affect velocity as well
    /// </summary>
    public Vector2 _offset;  //Used for Treadmill and Secondary External Velocities
    //public Vector2 offset { get { return _offset; } }

    public Vector2 acceleration;  
    public Vector2 deceleration;
    
    private Vector2 smoothing;           //Used as a reference for Mathf.Smooth
    private float timeScale;
    public float TimeScale { get { return (timeScale <= 0.001f) ? 1 : timeScale; } set { timeScale = value; } }

    public DampingVelocity(float acceleration) {
        this.acceleration = new Vector2(acceleration, acceleration);
        deceleration = this.acceleration;

        target = Vector2.zero;
        smoothing = Vector2.zero;
        _velocity = Vector2.zero;
        _offset = Vector2.zero;
        timeScale = 1;
    }
    public DampingVelocity(Vector2 acceleration) {
        this.acceleration = acceleration;
        deceleration = this.acceleration;

        target = Vector2.zero;
        smoothing = Vector2.zero;
        _velocity = Vector2.zero;
        _offset = Vector2.zero;
        timeScale = 1;
    }
    public DampingVelocity(Vector2 target, Vector2 acceleration) {
        this.target = target;
        this.acceleration = acceleration;
        deceleration = this.acceleration;

        smoothing = Vector2.zero;
        _velocity = Vector2.zero;
        _offset = Vector2.zero;
        timeScale = 1;
    }

    public void Update(float friction = 1, float drag = 0) {
        Vector2 scaledAcceleration = GetAcceleration() * TimeScale;
        Vector2 scaledTarget = (target + _offset)* TimeScale;

        //Update X Velocity     
        if(scaledAcceleration.x > 0) {
            if(friction != 0) {
                float responseTime = Mathf.Abs((scaledTarget.x - _velocity.x)/ scaledAcceleration.x )  / friction;   //Faster response with higher friction, slower with lower friction
                _velocity.x = Mathf.SmoothDamp(x, scaledTarget.x, ref smoothing.x, responseTime, float.MaxValue, Time.fixedDeltaTime * GeneticAlgorithmManager.deltaTime);
            }
        }
        else {
            _velocity.x = scaledTarget.x;
        }

        //Update Y Velocity
        if(scaledAcceleration.y > 0) {
            if(friction != 0) {
                float responseTime = Mathf.Abs((scaledTarget.y - _velocity.y)/ scaledAcceleration.y)  / friction;   //Faster response with higher friction, slower with lower friction
                _velocity.y = Mathf.SmoothDamp(y, scaledTarget.y, ref smoothing.y, responseTime, float.MaxValue, Time.fixedDeltaTime * GeneticAlgorithmManager.deltaTime);
            }
        }
        else {
            _velocity.y = scaledTarget.y;
        }

        //Apply Drag
        if(drag > 0) {
            Vector2 dragVelocity = DRAG_COEFFICIENT*drag*(new Vector2(_velocity.x*Mathf.Abs(_velocity.x), _velocity.y*Mathf.Abs(_velocity.y)));
            _velocity -= dragVelocity;
        }

        if(scaledTarget.x == _velocity.x)
            smoothing.x = 0;
        if(scaledTarget.y == _velocity.y)
            smoothing.y = 0;
        
        if(Mathf.Abs(x) < THRESHOLD)
            _velocity.x = 0;
        if(Mathf.Abs(y) < THRESHOLD)
            _velocity.y = 0;
    }
    Vector2 GetAcceleration() {
        Vector2 accel = Vector2.zero;
        if(Utility.FloatCompare(acceleration.x, deceleration.x) || _velocity.x == 0 || _velocity.x == target.x)
            accel.x = acceleration.x;
        else if(target.x == 0)
            accel.x = deceleration.x;
        else {
            int targetDirection = Mathf.RoundToInt(Mathf.Sign(target.x - _velocity.x));
            int zeroDirection = Mathf.RoundToInt(Mathf.Sign(-_velocity.x));
            accel.x = (targetDirection != zeroDirection) ? acceleration.x : deceleration.x;
        }

        if(Utility.FloatCompare(acceleration.y, deceleration.y) || _velocity.y == 0 || _velocity.y == target.y)
            accel.y = acceleration.y;
        else if(target.y == 0)
            accel.y = deceleration.y;
        else {
            int targetDirection = Mathf.RoundToInt(Mathf.Sign(target.y - _velocity.y));
            int zeroDirection = Mathf.RoundToInt(Mathf.Sign(-_velocity.y));
            accel.y = (targetDirection != zeroDirection) ? acceleration.y : deceleration.y;
        }

        return accel;
    }

    public void SetX(float xVelocity) {
        if(Mathf.Sign(x) != Mathf.Sign(xVelocity) || xVelocity == 0)
            smoothing.x = 0;

        _velocity.x = xVelocity;

        if(acceleration.x < 0.001f)
            target.x = xVelocity;
    }
    public void SetY(float yVelocity) {
        if(Mathf.Sign(y) != Mathf.Sign(yVelocity) || yVelocity == 0)
            smoothing.y = 0;

        _velocity.y = yVelocity;

        if(acceleration.y < 0.001f)
            target.y = yVelocity;
    }
    public void Set(Vector2 velocity) {
        if(Mathf.Sign(x) != Mathf.Sign(velocity.x) || velocity.x == 0)
            smoothing.x = 0;
        if(Mathf.Sign(y) != Mathf.Sign(velocity.y) || velocity.y == 0)
            smoothing.y = 0;

        _velocity.x = velocity.x;
        _velocity.y = velocity.y;

        if(acceleration.x < 0.001f)
            target.x =  velocity.x;
        if(acceleration.y < 0.001f)
            target.y = velocity.y;
    }

    public void AddX(float xVelocity) {
        _velocity.x += xVelocity;
    }
    public void AddY(float yVelocity) {
        _velocity.y += yVelocity;
    }
    public void Add(Vector2 velocity) {
        _velocity += velocity;
    }

    public void AddOffsetX(float offsetX, bool updateVelocity = false) {
        if(updateVelocity)
            _velocity.x += offsetX;
        this._offset.x += offsetX;
    }
    public void AddOffsetY(float offsetY, bool updateVelocity = false) {
        if(updateVelocity)
            _velocity.y += offsetY;
        this._offset.y += offsetY;
    }
    public void AddOffset(Vector2 offset, bool updateVelocity = false) {
        if(updateVelocity)
            _velocity += offset;
        this._offset += offset;
    }

    //public void SetOffsetX(float offsetX, bool updateVelocity = false) {
    //    if(updateVelocity) {
    //        _velocity.x -= this._offset.x;
    //        _velocity.x += offsetX;
    //    }
    //    this._offset.x = offsetX;
    //}
    //public void SetOffsetY(float offsetY, bool updateVelocity = false) {
    //    if(updateVelocity) {
    //        _velocity.y -= this._offset.y;
    //        _velocity.y += offsetY;
    //    }
    //    this._offset.y = offsetY;
    //}
    //public void SetOffset(Vector2 offset, bool updateVelocity = false) {
    //    if(updateVelocity) {
    //        _velocity -= this._offset;
    //        _velocity += offset;
    //    }
    //    this._offset = offset;
    //}    
}
