/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MultistateCollider : MonoBehaviour {
    [SerializeField][HideInInspector]
    private int currentState = 0;
    public int CurrentState { get { return currentState; }
        set { if (value >= states.Count) currentState = states.Count-1; else if (value < 0) currentState = 0; else currentState = value; } }
    public Vector2 offset { get { return states[currentState].offset; } }
    public Vector2 size { get { return states[currentState].size; } }
    public List<ColliderData> states; 

    public Bounds bounds { get { return new Bounds(offset, size); } }

    void Reset() {
        CustomRigidbody rb = GetComponent<CustomRigidbody>();
        if (rb)
            rb.multistateCollider = this;
        BoxCollider2D prev = GetComponent<BoxCollider2D>();
        states = new List<ColliderData>();
        if (prev) 
            states.Add(new ColliderData("Default", prev.size, prev.offset));        
        else
            states.Add(new ColliderData("Default"));
    } 

    public void SetSize(Vector2 size) {
        ColliderData data = states[CurrentState];
        data.size = size;
        states[CurrentState] = data;
    }
    public void SetOffset(Vector2 offset) {
        ColliderData data = states[CurrentState];
        data.offset = offset;
        states[CurrentState] = data;
    }
    
    /// <summary>
    /// Allows for modifying collider size while keeping the bottom edge in the same position.
    /// Collider cannot have a size of 0. 0 in x or y will revert collider to that axis default size.
    /// </summary>
    /// <param name="size"></param>
    public void SwapColliderState(int stateIndex = 0, bool alignToBottom = true) {
        if(CurrentState == stateIndex) return;

        ColliderData previous = states[CurrentState];
        CurrentState = stateIndex;

        if(alignToBottom) {
            Vector2 sizeChange = size - previous.size;
            sizeChange.x = 0;
            sizeChange /= 2;
            sizeChange.y += previous.offset.y - offset.y;
            this.transform.Translate(sizeChange);
        }
    }
    public void SwapColliderState(string stateName, bool alignToBottom = true) {
        if(states[CurrentState].name == stateName) return;

        int index = states.FindIndex(x => x.name == stateName);
        SwapColliderState(index, alignToBottom);
    }    
}

[Serializable]
public struct ColliderData {
    public string name;
    public Vector2 offset;
    public Vector2 size;

    public ColliderData(string name) {
        this.name = name;
        offset = Vector2.zero;
        size = Vector2.one;
    }
    public ColliderData(string name, Vector2 size) {
        this.name = name;
        offset = Vector2.zero;
        this.size = size;
    }
    public ColliderData(string name, Vector2 size, Vector2 offset) {
        this.name = name;
        this.offset = offset;
        this.size = size;
    }
}
