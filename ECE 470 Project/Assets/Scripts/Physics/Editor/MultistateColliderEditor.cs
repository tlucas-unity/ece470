/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MultistateCollider))]
public class MultistateColliderEditor : Editor {

    private bool editing = false;
    MultistateCollider multistateCollider;
    Tool previousTool = Tool.None;

    void OnEnable() {
        multistateCollider = (MultistateCollider)target;
    }

    void OnSceneGUI() {
        Handles.matrix = multistateCollider.transform.localToWorldMatrix;
        Handles.color = new Color(0.25f, .5f, 1);
        Handles.DrawWireCube(multistateCollider.offset, multistateCollider.size);
        if (editing) {
            Bounds bounds = multistateCollider.bounds;
            //bounds.center -= fake.transform.position;
            Vector3 position = new Vector3(bounds.min.x, bounds.center.y, multistateCollider.transform.position.z);
            Vector3 moved;
            EditorGUI.BeginChangeCheck();
            moved = Handles.FreeMoveHandle(position, Quaternion.identity, HandleUtility.GetHandleSize(position)*0.1f, Vector2.one, Handles.CubeHandleCap);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(multistateCollider, "Changed");
                Vector3 delta = position - moved;
                Vector2 current = multistateCollider.states[multistateCollider.CurrentState].size;
                current.x += delta.x;
                delta /= 2;
                Vector2 currentOffset = multistateCollider.states[multistateCollider.CurrentState].offset;
                currentOffset.x -= delta.x;
                multistateCollider.SetSize(current);
                multistateCollider.SetOffset(currentOffset);
            }
            position.x = bounds.max.x;
            EditorGUI.BeginChangeCheck();
            moved = Handles.FreeMoveHandle(position, Quaternion.identity, HandleUtility.GetHandleSize(position)*0.1f, Vector2.one, Handles.CubeHandleCap);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(multistateCollider, "Changed");
                Vector3 delta = moved - position;
                Vector2 current = multistateCollider.states[multistateCollider.CurrentState].size;
                current.x += delta.x;
                delta /= 2;
                Vector2 currentOffset = multistateCollider.states[multistateCollider.CurrentState].offset;
                currentOffset.x += delta.x;
                multistateCollider.SetSize(current);
                multistateCollider.SetOffset(currentOffset);
            }
            position.x = bounds.center.x;
            position.y = bounds.max.y;
            EditorGUI.BeginChangeCheck();
            moved = Handles.FreeMoveHandle(position, Quaternion.identity, HandleUtility.GetHandleSize(position)*0.1f, Vector2.one, Handles.CubeHandleCap);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(multistateCollider, "Changed");
                Vector3 delta = moved - position;
                Vector2 current = multistateCollider.states[multistateCollider.CurrentState].size;
                current.y += delta.y;
                delta /= 2;
                Vector2 currentOffset = multistateCollider.states[multistateCollider.CurrentState].offset;
                currentOffset.y += delta.y;
                multistateCollider.SetSize(current);
                multistateCollider.SetOffset(currentOffset);
            }
            position.y = bounds.min.y;
            EditorGUI.BeginChangeCheck();
            moved = Handles.FreeMoveHandle(position, Quaternion.identity, HandleUtility.GetHandleSize(position)*0.1f, Vector2.one, Handles.CubeHandleCap);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(multistateCollider, "Changed");
                Vector3 delta = position - moved;
                Vector2 current = multistateCollider.states[multistateCollider.CurrentState].size;
                current.y += delta.y;
                delta /= 2;
                Vector2 currentOffset = multistateCollider.states[multistateCollider.CurrentState].offset;
                currentOffset.y -= delta.y;
                multistateCollider.SetSize(current);
                multistateCollider.SetOffset(currentOffset);
            }
            position = bounds.center;
            EditorGUI.BeginChangeCheck();
            moved = Handles.FreeMoveHandle(position, Quaternion.identity, HandleUtility.GetHandleSize(position)*0.15f, Vector2.one, Handles.CubeHandleCap);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(multistateCollider, "Changed");
                multistateCollider.SetOffset(moved);
            }
        }
    }

    public override void OnInspectorGUI() {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(115));
        GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
        buttonStyle.padding = new RectOffset(3, 3, 4, 4);
        Color previous  = GUI.contentColor;
        if (editing) {
            buttonStyle.normal = GUI.skin.button.onNormal;
            GUI.contentColor = Color.white;
        }
        else {
            GUI.contentColor = new Color(0.3f, 0.3f, 0.3f);
        }

        if (GUILayout.Button("", buttonStyle, GUILayout.MaxHeight(25), GUILayout.MaxWidth(34), GUILayout.MinWidth(34))) {
            editing = !editing;
            if (editing) {
                if (previousTool == Tool.None) {
                    previousTool = Tools.current;
                    Tools.current = Tool.None;
                }
            }
            else {
                if (previousTool != Tool.None) {
                    Tools.current = previousTool;
                    previousTool = Tool.None;
                }
            }
            SceneView.RepaintAll();
            //int newState = (fake.CurrentState == 0)?1:0;                
            //fake.gameObject.GetComponent<CustomRigidbody>().SwapColliderState(newState, (newState != 1));
        }
        EditorGUILayout.BeginVertical();
        GUILayout.Space(6);
        EditorGUILayout.LabelField("Edit Collider");
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        GUILayout.Space(3);
        GUI.contentColor = previous;
        GUILayout.BeginHorizontal();
        GUILayout.Label("Current State", GUILayout.MaxWidth(115));

        string[] stateNames = new string[multistateCollider.states.Count];
        for (int i = 0; i < stateNames.Length; i++)
            stateNames[i] = multistateCollider.states[i].name;
        EditorGUI.BeginChangeCheck();
        multistateCollider.CurrentState = EditorGUILayout.Popup(multistateCollider.CurrentState, stateNames);
        if(EditorGUI.EndChangeCheck()){
            SceneView.RepaintAll();
        }
        GUILayout.EndHorizontal();

        base.OnInspectorGUI();
    }

    void OnDisable() {
        if (editing) {
            if (previousTool != Tool.None) {
                Tools.current = previousTool;
                previousTool = Tool.None;
            }
        }
    }
}

