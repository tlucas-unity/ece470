using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Chromosome {

    public int ChromosomeSize { get; private set; }
    public int[] genes;

    public Chromosome(int chromosomeSize) {
        int minGenes = (chromosomeSize / 32)+1;
        genes = new int[minGenes];
        this.ChromosomeSize = chromosomeSize;
    }

    /// <summary>
    /// Scramble is used when creating a brand new agent. It will completely randomize all the genes
    /// </summary>
    public void Scramble() {
        for(int i = 0; i < ChromosomeSize; i++) {
            int geneArrayIndex = (i / 32);
            int geneIndex = i % 32;

            int randomValue = Random.Range(0, 2);
            if (randomValue != 0)
                genes[geneArrayIndex] |= 1 << geneIndex;
            else
                genes[geneArrayIndex] &= ~(1 << geneIndex);
        }
    }

    /// <summary>
    /// Randomly mutates a few genes
    /// </summary>
    /// <param name="numberOfGenes">How many genes can be mutated (The same gene can be mutated multiple times but unlikely</param>
    /// <param name="chancePerGene">Value should be from 0.0f to 1.0f (1.0 being 100%)</param>
    public void Mutate(int numberOfGenes, float chancePerGene) {
        for (int i = 0; i < numberOfGenes; i++) {
            float randomValue = Random.Range(0.0f, 1.0f);
            if (randomValue <= chancePerGene) {
                int target = Random.Range(0, ChromosomeSize);
                int geneArrayIndex = target / 32;
                int geneIndex = target % 32;

                if ((genes[geneArrayIndex] & 1 << geneIndex) != 0) {    //is the gene a 1
                    genes[geneArrayIndex] &= ~(1 << geneIndex); //make 0
                }
                else {
                    genes[geneArrayIndex] |= 1 << geneIndex;    //make 1
                }
            }
        }
    }

    /// <summary>
    /// Isolates a particular gene in the chromosome
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public bool this[int i] {
        get {
            int geneArrayIndex = (i / 32);
            int geneIndex = i % 32;
            
            return ((genes[geneArrayIndex] & 1 << geneIndex) != 0);
        }
        set {
            int geneArrayIndex = (i / 32);
            int geneIndex = i % 32;

            if(value)
                genes[geneArrayIndex] |= 1 << geneIndex;
            else
                genes[geneArrayIndex] &= ~(1 << geneIndex);
        }
    }
}
