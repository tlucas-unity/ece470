using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MiscExtensions {   

    public static RectOffset Scale(this RectOffset ro, float scale) {
        return new RectOffset(Mathf.RoundToInt(ro.left*scale),
                Mathf.RoundToInt(ro.right*scale),
                Mathf.RoundToInt(ro.top*scale),
                Mathf.RoundToInt(ro.bottom*scale));
    }
    
    public static Vector2[] GetCorners(this Rect rect) {
        Vector2[] corners = new Vector2[4];
        corners[0] = new Vector2(rect.xMin, rect.yMin);
        corners[1] = new Vector2(rect.xMax, rect.yMin);
        corners[2] = new Vector2(rect.xMax, rect.yMax);
        corners[3] = new Vector2(rect.xMin, rect.yMax);
        return corners;
    }
    public static Vector2[] GetCorners(this Rect rect, Vector2 offset) {
        Vector2[] corners = new Vector2[4];
        corners[0] = new Vector2(rect.xMin + offset.x, rect.yMin + offset.y);
        corners[1] = new Vector2(rect.xMax + offset.x, rect.yMin + offset.y);
        corners[2] = new Vector2(rect.xMax + offset.x, rect.yMax + offset.y);
        corners[3] = new Vector2(rect.xMin + offset.x, rect.yMax + offset.y);
        return corners;
    }
}
