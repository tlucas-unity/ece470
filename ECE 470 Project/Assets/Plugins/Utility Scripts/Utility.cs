
using UnityEngine;
using System.Collections.Generic;

public static class Utility {
    const float DEFAULT_TOLERENCE = 0.0001f;

    public static string RemoveCloneSuffix(string name) {
        return name.Substring(0, name.Length - 7); //Removes (Clone)        
    }

    #region Vector3
    public static bool Compare(this Vector3 original, Vector3 target, float tolerence = DEFAULT_TOLERENCE) {
        bool condition = true;
        if (tolerence < 0) {
            Debug.LogError("Tolerence for VectorCompare is negative!");
            return false;
        }
        if (original.x > target.x + tolerence || original.x < target.x - tolerence)
            condition = false;
        if (original.y > target.y + tolerence || original.y < target.y - tolerence)
            condition = false;
        if (original.z > target.z + tolerence || original.z < target.z - tolerence)
            condition = false;

        return condition;
    }

    public static Vector3 Multiply(this Vector3 original, Vector3 scale) {
        Vector3 newVector = original;
        newVector.x *= scale.x;
        newVector.y *= scale.y;
        newVector.z *= scale.z;
        return newVector;
    }
    public static Vector3 Divide(this Vector3 original, Vector3 scale) {
        Vector3 newVector = original;
        newVector.x /= scale.x;
        newVector.y /= scale.y;
        newVector.z /= scale.z;
        return newVector;
    }
    public static Vector3 SwapXY(this Vector3 original) {
        return new Vector3(original.y, original.x, original.z);
    }

    public static Vector3 ToInt(this Vector3 original) {
        original.x = Mathf.RoundToInt(original.x);
        original.y = Mathf.RoundToInt(original.y);
        original.z = Mathf.RoundToInt(original.z);
        return original;
    }
    public static Vector3 ToFloor(this Vector3 original, bool trueFloor = false) {
        Vector3 floored = original;
        if (!trueFloor) {
            floored.x = Mathf.FloorToInt(floored.x);
            floored.y = Mathf.FloorToInt(floored.y);
            floored.z = Mathf.FloorToInt(floored.z);
        }
        else {
            if (floored.x > 0)
                floored.x = Mathf.FloorToInt(floored.x);
            else
                floored.x = Mathf.CeilToInt(floored.x);

            if (floored.y > 0)
                floored.y = Mathf.FloorToInt(floored.y);
            else
                floored.y = Mathf.CeilToInt(floored.y);

            if (floored.z > 0)
                floored.z = Mathf.FloorToInt(floored.z);
            else
                floored.z = Mathf.CeilToInt(floored.z);
        }
        return floored;
    }
    public static Vector3 ToCeiling(this Vector3 original, bool trueCeiling = false) {
        Vector3 floored = original;
        if (!trueCeiling) {
            floored.x = Mathf.CeilToInt(floored.x);
            floored.y = Mathf.CeilToInt(floored.y);
            floored.z = Mathf.CeilToInt(floored.z);
        }
        else {
            if (floored.x > 0)
                floored.x = Mathf.CeilToInt(floored.x);
            else
                floored.x = Mathf.FloorToInt(floored.x);

            if (floored.y > 0)
                floored.y = Mathf.CeilToInt(floored.y);
            else
                floored.y = Mathf.FloorToInt(floored.y);

            if (floored.z > 0)
                floored.z = Mathf.CeilToInt(floored.z);
            else
                floored.z = Mathf.FloorToInt(floored.z);
        }
        return floored;
    }

    /// <summary>
    /// Determine the signed angle between two vectors, with normal 'n'
    /// as the rotation axis.
    /// </summary>
    public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n) {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }
    #endregion
    #region Vector2 
    public static bool Compare(this Vector2 original, Vector2 target, float tolerence = DEFAULT_TOLERENCE) {
        if (tolerence< 0) {
            Debug.LogError("Tolerence for VectorCompare is negative!");
            return false;
        }
        if (original.x > target.x + tolerence || original.x<target.x - tolerence)
            return false;
        if (original.y > target.y + tolerence || original.y<target.y - tolerence)
            return false;

        return true;
    }
    public static Vector2 ToFloor(this Vector2 original) {
        Vector2 floored = original;
        floored.x = Mathf.FloorToInt(floored.x);
        floored.y = Mathf.FloorToInt(floored.y);
        return floored;
    }
    public static bool PolyContainsPoint(Vector2[] polyPoints, Vector2 point, out int bork) { 
        int j = polyPoints.Length-1;
        bool inside = false;
        bork = -1;
        for (int i = 0; i < polyPoints.Length; j = i++) { 
        if ( ((polyPoints[i].y <= point.y && point.y<polyPoints[j].y) || (polyPoints[j].y <= point.y && point.y<polyPoints[i].y)) && 
            (point.x< (polyPoints[j].x - polyPoints[i].x) * (point.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x)) 
            inside = !inside;
            bork = i;
        } 
        return inside; 
    }
    public static float AngleSigned(Vector2 v1, Vector2 v2) {
        return AngleSigned(v1, v2, Vector3.forward);
    }
    #endregion

    public static Rect Extend(this Rect rect, float value) {
        rect.x -= value;
        rect.y -= value;
        rect.width += value*2;
        rect.height += value;
        return rect;
    }

    /// <summary>
    /// Default Contains positions the 0, 0 or origin of the rect on the top left. This function fixes that place the origin on the bottom left
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    public static Rect ToWorldValues(this Rect rect) {
        Rect worldRect = new Rect();
        worldRect.min = new Vector2(rect.x - rect.width, rect.y - rect.height);
        worldRect.max = new Vector2(rect.x + rect.width, rect.y + rect.height);
        return worldRect;
    } 

    public static Rigidbody2D SetupRB(MonoBehaviour mono) {
        Rigidbody2D rigidbody = mono.GetComponent<Rigidbody2D>();
        rigidbody.bodyType = RigidbodyType2D.Kinematic;
        rigidbody.simulated = true;
        rigidbody.useFullKinematicContacts = true;
        return rigidbody;
    }

    /// <summary>
    /// Returns the sign of the float. Possible return values = (-1, 0, 1)
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public static int Sign(this float x) {
        if (x <= -0.0001f)
            return -1;
        else if (x >= 0.0001f)
            return 1;
        
        return 0;
    }
    public static bool FloatCompare(float x, float y, float tolerence = DEFAULT_TOLERENCE) {
        bool condition = true;
        if (tolerence < 0) {
            Debug.LogError("Tolerence for VectorCompare is negative!");
            return false;
        }
        if (x > y + tolerence || x < y - tolerence)
            condition = false;
        return condition;
    }
    public static Vector3 ToVector3(this Vector2 targetVector) {
        return new Vector3(targetVector.x, targetVector.y, 0);
    }
    public static Vector2 ToVector2(this Vector3 targetVector) {
        return new Vector2(targetVector.x, targetVector.y);
    }
    public static bool VectorContains(List<Vector3> vector3List, Vector3 target, float tolerence) {
        foreach (Vector3 vector in vector3List) {
            if (Compare(vector, target, tolerence)) {
                return true;
            }
        }
        return false;
    }

    #region Matrix4x4
    /// <summary>
    /// Matrix Math
    /// </summary>
    /// <param name="matrix"></param>
    /// <returns></returns>
    public static Quaternion GetRotation(this Matrix4x4 matrix) {
        var qw = Mathf.Sqrt(1f + matrix.m00 + matrix.m11 + matrix.m22) / 2;
        var w = 4 * qw;
        var qx = (matrix.m21 - matrix.m12) / w;
        var qy = (matrix.m02 - matrix.m20) / w;
        var qz = (matrix.m10 - matrix.m01) / w;

        return new Quaternion(qx, qy, qz, qw);
    }

    /// <summary>
    /// Matrix Math
    /// </summary>
    /// <param name="matrix"></param>
    /// <returns></returns>
    public static Vector3 GetPosition(this Matrix4x4 matrix) {
        var x = matrix.m03;
        var y = matrix.m13;
        var z = matrix.m23;

        return new Vector3(x, y, z);
    }

    /// <summary>
    /// Matrix Math
    /// </summary>
    /// <param name="m"></param>
    /// <returns></returns>
    public static Vector3 GetScale(this Matrix4x4 m) {
        Vector3 scale;
        scale.x = new Vector4(m.m00, m.m10, m.m20, m.m30).magnitude;
        scale.y = new Vector4(m.m01, m.m11, m.m21, m.m31).magnitude;
        scale.z = new Vector4(m.m02, m.m12, m.m22, m.m32).magnitude;
        return scale;
    }
    #endregion

    public static Vector2 RadianToVector2(float radian) {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }

    public static Vector2 DegreeToVector2(float degree) {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }

    public static Rect CombineRectXY(this Rect r1, Rect r2) {
        return new Rect(r1.x+r2.x, r1.y+r2.y, 0, 0);
    }
    
    public static bool Contains(this LayerMask mask, Collider2D collider) {
        return ((mask & (1<<collider.gameObject.layer)) > 0);
    }
}

