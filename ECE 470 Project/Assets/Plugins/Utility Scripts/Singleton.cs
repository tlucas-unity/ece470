using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public abstract class SSingleton<T> : MonoBehaviour where T : MonoBehaviour {
    public static T Instance { get; private set; }
    
    protected void Register(T self) {
        Instance = self;
    }

    //#if UNITY_EDITOR
    ///// <summary>
    ///// Function is slow, and only available for the editor
    ///// </summary>
    //public static void ExternalRegister() {
    //    Instance = GameObject.FindObjectOfType<T>();
    //}
    //#endif
}