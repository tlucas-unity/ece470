/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comment : MonoBehaviour {
#pragma warning disable 0414
    [TextArea][SerializeField]
    private string comment = "";
#pragma warning restore 0414

#if UNITY_EDITOR
    private void Reset() {
        if(this.gameObject.tag != "EditorOnly") {
            Debug.LogWarning("Comments should only be attached to a GameObject with \"EditorOnly\" tag");
            this.gameObject.tag = "EditorOnly";
        }
    }
#endif  
}
