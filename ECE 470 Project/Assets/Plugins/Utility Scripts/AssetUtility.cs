using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class AssetUtility {
#if UNITY_EDITOR
    /// <summary>
    ///	This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    public static T CreateAsset<T>() where T : ScriptableObject {
        T asset = ScriptableObject.CreateInstance<T>();

        //string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        //if(path == "") {
        //    path = "Assets";
        //}
        //else if(Path.GetExtension(path) != "") {
        //    path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        //}

        //string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

        //AssetDatabase.CreateAsset(asset, assetPathAndName);

        ProjectWindowUtil.CreateAsset(asset, "New " + typeof(T).Name + ".asset");

        EditorUtility.SetDirty(asset);
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;

        return asset;
    }
#endif

    public static void SaveJson(string json, string fileName = "World.json") {
        string path = Application.dataPath + "/Resources/Files/" + fileName;
        //string path = Application.persistentDataPath + "/My Worlds/" + fileName;
        File.WriteAllText(path, json);
#if UNITY_EDITOR
        AssetDatabase.Refresh();
#endif
    }

    public static string LoadJson() {
        TextAsset jsonAsset = Resources.Load("Files/World") as TextAsset;
        return jsonAsset == null ? null : jsonAsset.text;
    }

    #region JSON Scripts
    /// <summary>
    /// If you want to complete, use something like to make sure the json is serializing everything properly
    /// Last I checked, Vector2's weren't working, and SerializeObject wasn't being filtered
    /// 
    //TestObject obj = new TestObject();

    ////JsonSerializeThis obj = new JsonSerializeThis();

    //string json = JsonUtility.ToJson(obj, true);
    ////string myJson = TestObject.ToJson(obj);
    //AssetUtility.SaveJson(json);
    //    //AssetUtility.SaveJson(myJson, "myJson.json");    
    /// </summary>
    /// <param name="target"></param>
    /// <param name="includeSerializableObject"></param>
    /// <returns></returns>
    public static string ToJson(object target, bool includeSerializableObject = false) {
        throw new NotImplementedException();        
        //return InternalToJson(target, 0, includeSerializableObject);
    }

    private static string InternalToJson(object target, int indentLevel, bool includeSerializableObject = false) {
        if(target == null)
            return "{ }";

        //Get Fields
        FieldInfo[] variable = target.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

        string json = NewLine(indentLevel-1) + "{"; //Starts the Object

        //If "target" is an Object just get its instance ID and move on (do not serialize its data)
        #region UNITY_OBJECT
        if(target.GetType().BaseType == typeof(UnityEngine.Object)) {
            json += NewLine(indentLevel) + "\"instanceID\": ";
            if(target != null)
                json += (target as UnityEngine.Object).GetInstanceID();
            else
                json += "0";
        }
        #endregion
        #region Everything Else
        else {
            for(int i = 0; i < variable.Length; i++) {
                if(!IsSerializable(variable[i])) {  //Check if the field is even serializable or not
                    //Debug.Log(variable[i].Name);
                    continue;
                }

                json += NewLine(indentLevel) + "\"" + variable[i].Name + "\": ";    //Insert the variable name


                if(IsComplex(variable[i].FieldType)) {  //Is the variable a complex type? eg. does the variable contain more variables
                    #region UNITY_OBJECT
                    if(variable[i].FieldType.BaseType == typeof(UnityEngine.Object)) {
                        json += "{" + NewLine(indentLevel+1) + "\"instanceID\": ";
                        if(variable[i].GetValue(target) != null)
                            json += (variable[i].GetValue(target) as UnityEngine.Object).GetInstanceID();
                        else
                            json += "0";
                        json += NewLine(indentLevel) + " }";
                    }
                    #endregion 
                    #region SERIALIZABLE_OBJECT
                    #endregion
                    #region ARRAY
                    else if(variable[i].FieldType.IsArray) {
                        Array array = variable[i].GetValue(target) as Array;
                        json += "[";
                        if(array != null) {
                            if(IsComplex(array.GetType().GetElementType())) {
                                for(int x = 0; x < array.Length-1; x++) {
                                    json += InternalToJson(array.GetValue(x), indentLevel+2, includeSerializableObject) + ",";
                                }
                                json += InternalToJson(array.GetValue(array.Length-1), indentLevel+2, includeSerializableObject);
                            }
                            else if(array.GetType().GetElementType().BaseType == typeof(Enum)) {
                                for(int x = 0; x < array.Length-1; x++) {
                                    json +=  NewLine(indentLevel + 1) + (int)array.GetValue(x) + ",";
                                }
                                json += NewLine(indentLevel + 1) + (int)array.GetValue(array.Length-1);
                            }
                            else {
                                for(int x = 0; x < array.Length-1; x++) {
                                    json +=  NewLine(indentLevel + 1) + array.GetValue(x) + ",";
                                }
                                json += NewLine(indentLevel + 1) + array.GetValue(array.Length-1);
                            }
                            json += NewLine(indentLevel) +"]";
                        }
                        else {
                            json += "]";
                        }
                    }
                    else if(IsList(variable[i].FieldType)) {
                        IList list = variable[i].GetValue(target) as IList;

                        json += "[";
                        if(list != null) {
                            Type listType = list.GetType().GetGenericArguments()[0];
                            if(IsComplex(listType)) {
                                for(int x = 0; x < list.Count-1; x++) {
                                    json += InternalToJson(list[x], indentLevel+2, includeSerializableObject) + ",";
                                }
                                json += InternalToJson(list[list.Count-1], indentLevel+2, includeSerializableObject);
                            }
                            else if(listType.BaseType == typeof(Enum)) {
                                for(int x = 0; x < list.Count-1; x++) {
                                    json +=  NewLine(indentLevel + 1) + (int)list[x] + ",";
                                }
                                json += NewLine(indentLevel + 1) + (int)list[list.Count-1];
                            }
                            else {
                                for(int x = 0; x < list.Count-1; x++) {
                                    json +=  NewLine(indentLevel + 1) + list[x] + ",";
                                }
                                json += NewLine(indentLevel + 1) + list[list.Count-1];
                            }
                            json += NewLine(indentLevel) +"]";
                        }
                        else {
                            json += "]";
                        }
                    }
                    #endregion
                    #region BASE_OBJECT
                    else {
                        json += InternalToJson(variable[i].GetValue(target), indentLevel+1, includeSerializableObject);
                    }
                    #endregion
                }
                else if(variable[i].FieldType == typeof(string))    //If it is a string, added the quotes around it
                    json += "\"" + variable[i].GetValue(target) + "\"";
                else if(variable[i].FieldType.BaseType.IsEnum)  //If it is a enum convert to int value
                    json += (int)variable[i].GetValue(target);
                else
                    json += variable[i].GetValue(target);   //Else just add as is!

                if(i < variable.Length-1)
                    json += ",";
            }
        }
        #endregion

        json += (indentLevel < 1) ? "\n}" : NewLine(indentLevel-1) +"}";

        return json;
    }

    /// <summary>
    /// Creates a new line and indents it for correct formatting
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    static string NewLine(int level) {
        string indent = "\n";
        if(level < 0)
            return "";
        for(int i = 0; i <= level; i++) {
            indent += "\t";
        }
        return indent;
    }

    /// <summary>
    /// Checks all the conditions to see if the field is actually serializable
    /// </summary>
    /// <param name="variable"></param>
    /// <returns></returns>
    static bool IsSerializable(FieldInfo variable) {
        if((variable.Attributes & FieldAttributes.NotSerialized) > 0) { //If specifically told not to serialize
            return false;
        }
        if(variable.IsPrivate || variable.IsFamily) {   //If private or protected but has SerializeField
            object[] customAttributes = variable.GetCustomAttributes(typeof(SerializeField), false);
            if(customAttributes.Length == 0) {
                return false;
            }
        }
        //If class/struct is not marked with [System.Serializable] and is not an enum, unity object or SerializableObject
        if((variable.FieldType.Attributes & TypeAttributes.Serializable) == 0
           // && variable.FieldType.BaseType != typeof(SerializableObject)
            && variable.FieldType.BaseType != typeof(Enum)
            && variable.FieldType.BaseType != typeof(UnityEngine.Object)) {
            return false;
        }
        return true;
    }


    /// <summary>
    /// Checks to see if the type is a generic list
    /// </summary>
    /// <param name="typeIn"></param>
    /// <returns></returns>            
    static bool IsList(Type typeIn) {
        return (typeIn.IsGenericType && (typeIn.GetGenericTypeDefinition() == typeof(List<>)));
    }

    /// <summary>
    /// Is basic value or an object/struct
    /// </summary>
    /// <param name="typeIn"></param>
    /// <returns></returns>
    static bool IsComplex(Type typeIn) {
        return !IsSimple(typeIn);
        //if(typeIn.IsSubclassOf(typeof(System.ValueType)) || typeIn.Equals(typeof(string))) //|| typeIn.IsPrimitive
        //    return false;
        //else
        //    return true;
    }
    static bool IsSimple(Type type) {
        if(type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)) {
            // nullable type, check if the nested type is simple.
            return IsSimple(type.GetGenericArguments()[0]);
        }
        return type.IsPrimitive
          || type.IsEnum
          || type.Equals(typeof(string))
          || type.Equals(typeof(decimal));
    }
    #endregion
}
