﻿using UnityEngine;

public enum CommentBoxType { None, Info, Warning, Error }

public class CommentBoxAttribute : PropertyAttribute {

    public string text;
    public CommentBoxType messageType;

    public CommentBoxAttribute(string text, CommentBoxType messageType = CommentBoxType.None) {
        this.text = text;
        this.messageType = messageType;
    }
}