using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This will use the property you place, However it uses set = get (cause I'm lazy) so make sure it won't break anything
/// </summary>
public class GetSetPositionAttribute : PropertyAttribute {
}
