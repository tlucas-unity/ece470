/* Copyright (C) Must Be Made Games, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tristan Lucas <tlucas.u@gmail.com>, March 2017
 */ 
using UnityEngine;

public class BitMaskAttribute : PropertyAttribute {
    public System.Type propType;
    public BitMaskAttribute(System.Type aType) {
        propType = aType;
    }
}
