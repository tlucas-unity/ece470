using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PrintLog {
    private string header = "";
    private List<string> log = new List<string>();

    private string lastLine = "";    //Contents of last line (without timestamp)
    private string currentLine = ""; //Contents of the current line (without timestamp)

    public int maxLines;
    public bool addTimeStamp = false;
    public bool recording = true;
    public bool ignoreDuplicates = true;
    
    public PrintLog(int maxLines = 1000, bool addTimeStamp = false) {
        this.maxLines = maxLines;
        if (maxLines == 0)
            this.maxLines = int.MaxValue;

        this.addTimeStamp = addTimeStamp;
    }

    public void Add(string content) {
        if(!recording) return;
        currentLine += content;
    }

    public void AddLine(string content) {
        if(!recording) return;

        if(currentLine != "") {
            NextLine();
        }

        currentLine = content;

        NextLine();
    }

    public void NextLine() {    //Or End Line()
        if(!recording || currentLine == "") return;

        if(ignoreDuplicates && log.Count > 1 && currentLine == lastLine) {
            currentLine = "";
            return;
        }

        lastLine = currentLine;

        if(addTimeStamp) {
            currentLine = Time.time.ToString("N4") + "\t" + currentLine;
        }

        log.Add(currentLine);

        currentLine = "";

        if(log.Count > maxLines) {
            log.RemoveAt(0);
        }
    }

    public void SetHeader(string content) {
        header = content;
    }
    
    /// <summary>
    /// Automatically adds .txt and places the log in the /Asset/ Folder
    /// </summary>
    /// <param name="name"></param>
    public void PrintToTextFile(string name) {
        if(log.Count <= 0) {
            Debug.LogWarning(name + " Debug Log was empty");
            return;
        }
        // WriteAllLines creates a file, writes a collection of strings to the file,
        // and then closes the file.  You do NOT need to call Flush() or Close().
        string path = Application.dataPath;
        path += "/" + name + ".txt";
        path.Replace('/', '\\');

        List<string> printLog = new List<string>(log);

        if(header != "")
            printLog.Insert(0, header);

        System.IO.File.WriteAllLines(path, printLog.ToArray());

        //log.Clear();

        Debug.Log("Successfully printed to " + path);
        //Or Destroy the log?
        //log.Reverse();
    }
}
